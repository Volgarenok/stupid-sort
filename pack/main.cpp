#include <omp.h>
#include <iostream>
#include <cstddef>
struct entity
{
	size_t get() const;
	size_t key;
};
int main(int, char**)
{
	omp_get_wtime();
	return 0;
}

size_t entity::get() const
{
	return key;
}
